package app.nike.models;

public class Diploma {

	private long id;

	private String nomeDiploma;

	private String annoConseguimento;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNomeDiploma() {
		return nomeDiploma;
	}

	public void setNomeDiploma(String nomeDiploma) {
		this.nomeDiploma = nomeDiploma;
	}

	public String getAnnoConseguimento() {
		return annoConseguimento;
	}

	public void setAnnoConseguimento(String annoConseguimento) {
		this.annoConseguimento = annoConseguimento;
	}
	
	
}
