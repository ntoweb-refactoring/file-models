package app.nike.models;

public class Laurea {
	
	private long id;

	private String laurea;

	private String annoConseguimento;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLaurea() {
		return laurea;
	}

	public void setLaurea(String laurea) {
		this.laurea = laurea;
	}

	public String getAnnoConseguimento() {
		return annoConseguimento;
	}

	public void setAnnoConseguimento(String annoConseguimento) {
		this.annoConseguimento = annoConseguimento;
	}
	
	
}
